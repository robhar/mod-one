import { NgModule } from '@angular/core';
import { ModOneComponent } from './mod-one.component';

@NgModule({
  declarations: [ModOneComponent],
  imports: [
  ],
  exports: [ModOneComponent]
})
export class ModOneModule { }
