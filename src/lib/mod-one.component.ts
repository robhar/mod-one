import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'lib-mod-one',
  styleUrls: ['./mod-one.component.scss'],
  template: `
    <p class="one red">
      mod-one works!
    </p>
  `,
})
export class ModOneComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
